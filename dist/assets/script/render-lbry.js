var renderTarget = document.getElementById('render-target');
var width = renderTarget.offsetWidth
var cols = parseInt(width / 300)
var colWidth = width / cols - 20

function createCategory(category) {
  var h3 = document.createElement('h3');
  h3.innerText = category;
  renderTarget.appendChild(h3);
}

function createPreview(data) {
  var gridElement = document.createElement('div');
  gridElement.classList = 'lbry-element';
  gridElement.style.width = colWidth+'px';

  var preview = document.createElement('div');
  preview.id = data.claim_id;
  preview.setAttribute('data-name', data.normalized_name);
  var height = (colWidth * 0.5625);
  preview.style.height = height+'px';
  preview.classList = 'lbry-preview';
  preview.style.backgroundImage = data.thumbnail.url
  ? 'url('+data.thumbnail.url+')'
  : 'url(/assets/photo/europe-4262910_640.jpg)';
  preview.addEventListener('click', createEmbed);

  var title = document.createElement('div');
  title.classList = "video-title";
  title.innerText = data.title;

  var overlayWrapper = document.createElement('div');
  overlayWrapper.classList = 'overlay-wrapper';
  var playButton = document.createElement('ion-icon');
  playButton.name = "play-circle-outline";
  
  overlayWrapper.appendChild(playButton);
  preview.appendChild(overlayWrapper);
  gridElement.appendChild(preview);
  gridElement.appendChild(title);
  renderTarget.appendChild(gridElement);
}

function createEmbed(event) {
  var data = event.currentTarget;
  var iframe = document.createElement('iframe');
  iframe.src = 'https://lbry.tv/$/embed/'+data.getAttribute('data-name')+'/'+data.id+'?autoplay=true';
  iframe.classList = 'lbry-embed';
  iframe.width = colWidth;
  iframe.height = colWidth * 0.5625;
  iframe.allowFullscreen = true;
  event.currentTarget.parentNode.replaceChild(iframe, document.getElementById(data.id));
}

fetch('http://localhost:8000/api/video_categorized', {
  method: 'POST'
})
.then(res => res.json())
.then(res => {
  Object.entries(res).map(([category, list], i) => {
    createCategory(category);
    list.map(data => {
      createPreview(data);
    })
  });
  console.log(res);
});