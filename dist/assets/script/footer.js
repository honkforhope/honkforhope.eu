var section = location.pathname.split('/')[1];
var element = document.querySelector('[href="/'+section+'"');
element && element.classList.add('active');

var subsectionElement = document.querySelector('[href="'+location.pathname+'"');
subsectionElement && subsectionElement.classList.add('active');

function contentHeight () {
  return Math.max(
    document.body.scrollHeight, document.documentElement.scrollHeight,
    document.body.offsetHeight, document.documentElement.offsetHeight,
    document.body.clientHeight, document.documentElement.clientHeight
  );
}
var jumpToTop = document.getElementById('jump-to-top');
window.onscroll = function() {
  if (window.scrollY > contentHeight() - 250 - window.innerHeight) {
    jumpToTop.style.bottom = '270px';
  } else {
    jumpToTop.style.bottom = '0px';
  }
}

if (contentHeight() - 100 < window.outerHeight) {
  jumpToTop.style.display = 'none';
} else {
  jumpToTop.style.display = 'block';
}

function checkAlert() {
  fetch('/assets/alert.json')
  .then(function (res) {
    return res.json();
  })
  .then(function (alert) {
    console.log(alert);
    if (alert.active) {
      var ticker = document.getElementById('ticker');
      ticker.classList = 'ticker bg-'+alert.type;
      ticker.href = alert.link;
      document.getElementById('ticker-text').innerText = alert.text;
      document.body.classList = 'ticker-active';
    }
  })
  var checkInterval = setInterval(function () {
    clearInterval(checkInterval);
    checkAlert()
  }, 1000 * 30);
}

checkAlert();
