const { DaemonClient, CrdClient } = require('lbry')

const client = new DaemonClient('http://localhost:5279')

let claim_list_response;

const fetchClaimList = async () => {
  let result
  try {
    result = await client.claim_list({
      page_size: 1000
    })
  } catch (err) {
    console.log('[lbry-api]', err)
  }
  if (result.items) {
    console.log('[lbry-api] successfully (re)fetched claim_list')
    claim_list_response = result
  }
}

fetchClaimList()
setInterval(() => {
  fetchClaimList()
}, 1000 * 60 * 10)

const video_categorized = async () => {
  const { items: claim_list } = claim_list_response
  // console.log(list)
  const video_list = {}
  for (const claim of claim_list) {
    const { claim_id, normalized_name, value_type, value } = claim
    if ('stream' != value_type)
      continue
    const match = value && value.description && value.description.match(/(?<=Kategorie:\s{1,3})[^\s]*/)
    const category = match && match[0] || 'Allgemein'
    if (!video_list[category]) {
      video_list[category] = []
    }
    video_list[category].push({
      claim_id,
      normalized_name,
      ...value
    })    
  }
  return video_list
}

module.exports = {
  video_categorized
}