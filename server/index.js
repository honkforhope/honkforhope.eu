const app = require('express')()
const cors = require('cors')
const { video_categorized } = require('./controller')

app.use(cors({
  origin: 'http://192.168.178.21:5500'
  // origin: 'http://192.168.150.21:5500'
}))

app.post('/api/video_categorized', async (req, res) => {
  res.send(await video_categorized())
})


app.listen('8000', () => {
  console.log("[lbry-api] listening on port 8000.")
})