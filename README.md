## Installation
```shell
$ git clone git@gitlab.com:honkforhope/honkforhope.eu.git honkforhope.eu
$ cd honkforhope.eu
$ npm install -g sass nodemon concurrently
$ npm install
```

## Dev-Server
#### **Kompilieren**
```shell
$ npm start
# Kompiliert SCSS -> CSS und src/ -> /dist (server-side-includes)
# Wird bei Änderungen (Strg + S) automatisch ausgelöst.
```


#### **Live-Reload**
```shell
# VS Code Extension 'live-server' installieren, dann:

'Strg + ,' und nach 'liveServer.settings.multiRootWorkspaceName' suchen
Dort 'dist' eingeben. # Der live-server liefert dann diesen Ordner aus

$ code honkforhope.code-workspace # Workspace öffnen
Unten rechts auf 'Go Live' um live-server zu starten
```

#### **LBRY Backend**
```shell
# DEV
nodemon -w server/index.js -w server/controller.js server/index.js

# PRODUCTION
npm run lbry-api
```